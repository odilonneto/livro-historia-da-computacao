PRIMEIROS COMPUTADORES

Em 1890, o norte americano Hermann Hollerith (1860-1929) desenvolve o primeiro computador mecânico. A partir de 1930, começam as pesquisas para substituir as partes mecânicas por elétricas. O Mark I, concluído em 1944 por uma equipe liderada por Howard Aiken, é o primeiro computador eletromecânico capaz de efetuar cálculos mais complexos sem a interferência humana. Ele mede 15 m x 2,5 m e demora 11 segundos para executar um cálculo. Em 1946, surge o Eniac (Electronic Numerical Integrator and Computer), primeiro computador eletrônico e digital automático: pesa 30 toneladas, emprega cerca de 18 mil válvulas e realiza 4.500 cálculos por segundo. O Eniac contém a arquitetura básica de um computador, empregada até hoje: memória principal (área de trabalho), memória auxiliar (onde são armazenados os dados), unidade central de processamento (o "cérebro" da máquina, que executa todas as informações) e dispositivos de entrada e saída de dados que atualmente permitem a ligação de periféricos como monitor, teclado, mouse, scanner, tela, impressora, entre outros. A invenção do transistor, em 1947, substitui progressivamente as válvulas, aumentando a velocidade das máquinas.

IMPACTOS DA SOCIEDADE

O desenvolvimento da informática exerce um grande impacto no modo de produção da sociedade. O computador torna-se uma importante ferramenta de trabalho, contribuindo para o aumento da produtividade, redução de custos e melhoria da qualidade dos produtos. Vários setores da economia já estão informatizados, entre os quais a indústria, a pesquisa científica, a educação, o sistema financeiro, as comunicações e a astronáutica. Nas fábricas, os robôs substituem gradativamente a mão de obra humana em trabalhos que envolvem risco e em atividades mecânicas, como as linhas de produção e montagem. Essa automação progressiva é a causa da eliminação de vários postos de trabalho como caixas de banco, telefonistas e datilógrafos, tendência que é conhecida por desemprego estrutural. Por outro lado, a informática também cria novas categorias de profissionais, cuja principal característica é o domínio das tecnologias da atualidade.
Na pesquisa científica, o computador tem possibilitado a simulação de experiências inviáveis na realidade, devido ao alto custo ou periculosidade, como situações de temperatura excessiva ou de explosões. Na educação há uma grande variedade de softwares que ensinam Desenho, Música ou Gramática. Os bancos oferecem um número cada vez maior de serviços informatizados, como os caixas eletrônicos e as consultas on-line a partir de um computador pessoal ligado à agência. Na área das comunicações, a grande inovação é a interligação dos computadores de todo o mundo numa grande rede, a Internet. Na
astronáutica, os satélites artificiais informatizados fazem, entre outras aplicações, o mapeamento da atmosfera terrestre e de outros planetas.

RESUMO DOS PRINCIPAIS FATOS

-XVII - O francês Blaise Pascal projeta uma calculadora que soma e subtrai e o alemão Gottfried Wilhelm Leibniz incorpora operações de multiplicar e dividir à máquina.
-XVIII - O francês Joseph Marie Jacquard constrói um tear automatizado: cartões perfurados controlam o movimento da máquina.
-1834 - O inglês Charles Babbage projeta a máquina analítica capaz de armazenar informações.
-1847 - O inglês George Boole estabelece a lógica binária para armazenar informações.
-1890 - O norte-americano Hermann Hollerith constrói o primeiro computador mecânico.
-1924 - Nasce a International Business Machines Corporation (IBM), nos Estados Unidos.
-1938 - O alemão Konrad Zuse faz o primeiro computador elétrico usando a teoria binária.
-1943 - O inglês Alan Turing constrói a primeira geração de computadores modernos, que utilizam válvulas.
-1944 - O norte-americano Howard Aiken termina o Mark I, o primeiro computador eletromecânico.
-1946 - O Eletronic Numerical Integrator and Computer (Eniac), primeiro computador eletrônico, é criado nos EUA.
-1947 - Criação do transistor, substituto da válvula, que permite máquinas mais rápidas.
-1957 - Primeiros modelos de computadores transistorizados chegam ao mercado.
-1958 - Criação do chip, circuito integrado que permite a miniaturização dos equipamentos eletrônicos.
-1969 - Criação da Arpanet, rede de informações do Departamento de Defesa norte-americano interligando universidades e empresas, que dará origem à Internet.
-1974 - A Intel projeta o microprocessador 8080, que origina os microcomputadores.
-1975 - Os norte-americanos Bill Gates e Paul Alen fundam a Microsoft.
-1976 - Lançamento do Apple I, primeiro microcomputador comercial, inventado por Steves Jobs e por Steves Woznick.
-1981 - A IBM o lança seu microcomputador - o PC - com o sistema operacional MS-DOS, elaborado pela Microsoft.
-1983 - A IBM lança o PC-XT, com disco rígido.
-1984 - A National Science Foundation, nos Estados Unidos, cria a Internet, rede mundial de computadores que conecta governos, universidades e companhias.
-1984 -- A Apple lança o Macintosh, primeiro computador a utilizar ícones e mouse.
-1985 - A Microsoft lança o Windows para o PC, que só obtém sucesso com a versão 3.0 (1990).
-1993 - A Intel lança o Pentium.
-1998 - A Intel lança o Pentium II.
-1999 - A Intel lança o Pentium III.

REFERÊNCIAS

https://siteantigo.portaleducacao.com.br
https://blog.certisign.com.br
https://brasilescola.uol.com.br 
