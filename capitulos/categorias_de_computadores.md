# Categorias de Computadores

Os computadores podem ser classificados pelo porte. Existem os de grande porte, mainframes, médio porte, minicomputadores e pequeno porte microcomputadores, divididos em duas categorias: os de mesa (desktops) e os portáteis (notebooks e handhelds).

Conceitualmente todos eles realizam funções internas idênticas, mas em escalas diferentes.

Os Mainframes se destacam por terem alto poder de processamento e muita capacidade de memória, e controlam atividades com grande volume de dados, sendo de custo bastante elevado. Operam em MIPS (milhões de instruções por segundo).

A classificação de um determinado computador pode ser feita de diversas maneiras, como por exemplo em termos de:

- capacidade de processamento;
- velocidade de processamento e volume de transações;
- capacidade de armazenamento das informações;
- sofisticação do software disponível e compatibilidade;
- tamanho da memória e tipo de UCP.

Os microcomputadores de mesa, são os mais utilizados no mercado de um modo geral, pois atendem a uma infinidade de aplicações; são divididos em duas plataformas: PC, os computadores pessoais da IBM e Macintosh da Apple. Os dois padrões de micros têm diversos modelos, configurações e opcionais.


# Referências:

1. fundacaobradesco.org.br
