# Microcomputadores

Um microcomputador é um computador projetado para uso individual. O termo foi introduzido nos 1970s para diferenciar sistemas de computadores de mesa de grandes minicomputadores. É frequentemente usado como sinônimo do termo "computador desktop, "mas pode se referir a um servidor or laptop tão bem.

Nos 1960s e 1970s, os computadores eram muito maiores do que hoje, geralmente ocupando vários metros cúbicos de espaço. Alguns estrutura principal computadores podem até encher uma sala grande. Portanto, os primeiros computadores que cabiam em uma área de trabalho foram rotulados apropriadamente de "microcomputadores" em comparação com essas máquinas maiores.

Os primeiros microcomputadores foram disponibilizados nos 1970s e foram utilizados principalmente pelas empresas. À medida que se tornaram mais baratos, os indivíduos puderam comprar seus próprios sistemas de microcomputadores. Isso levou à revolução dos computadores pessoais dos 1980s, nos quais os microcomputadores se tornaram um produto de consumo comum.

À medida que os microcomputadores cresceram em popularidade, o nome "microcomputador" desapareceu e foi substituído por outros termos mais específicos. Por exemplo, os computadores adquiridos para fins comerciais foram rotulados como estações de trabalho, enquanto os computadores comprados para uso doméstico ficaram conhecidos como computadores pessoais, ou PCs. Eventualmente, os fabricantes de computadores desenvolveram computadores portáteis, chamados laptops. Embora os computadores tenham evoluído muito nas últimas décadas, esses mesmos termos ainda são usados ​​hoje.

# Referências:

1. TechLib - O dicionario de informatica Lib Tech
