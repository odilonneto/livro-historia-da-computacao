# História dos Computadores 

A palavra “computador” vem do latim “computator” que, por sua vez, significa “calcular”. Nesse sentido, notamos que os computadores surgiram na idade antiga, onde a relação de contar já intrigavam os homens. Dessa forma, iniciaram a criação das chamadas [calculadoras mecanicas](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/calculadoras_mecanicas.md), posteriormente, os [primeiros computadores](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/primeiros_computadores.md).



# Referências:
https://www.todamateria.com.br/historia-e-evolucao-dos-computadores/

https://www.tecmundo.com.br/tecnologia-da-informacao/1697-a-historia-dos-computadores-e-da-computacao.htm
