# Mainframe

Mainframe é um computador de grande porte dedicado ao processamento de grandes volumes de dados, com alto desempenho, performance, escalabilidade e segurança. Apesar de o título mainframe nos remeter à enorme estrutura centralizada onde eram executados os processamentos dos dados, ao longo dos anos esse servidor vem ocupando cada vez menor espaço físico e agregando maior capacidade computacional.

Em razão de operar com processadores especializados (CPs, IFLs, zIIPs, dentre outros), além de recursos de criptografia, compactação, IO de rede, IO de disco, processamento Java, processamento Linux, por exemplo, em uma mesma máquina, obtém melhores tempos no processamento transacional online ou batch pela eficiência na distribuição das tarefas, otimizando a entrega de resultados.


# Referências:

1. José Edson e Sandro (Serpro)