# Os Primeiros Aparelhos 
A primeira empresa que mostrou um aparelho funcionando foi a Motorola. O nome do aparelho era DynaTAC e não estava a vendo ao público. O primeiro modelo liberado comercialmente nos EUA, foi o DynaTAC 8000x, no ano de 1983, dez anos após o primeiro protótipo. 
A primeira geração de celulares pesava me média 1kg e tinha quase 30 centímetros de altura. O Preço dos celulares nessa época eram absurdos. 

# Segunda Geração 
No início da década de 90, as fabricantes já estavam prontas para lançar novos aparelhos, com um tamanho aceitável e novas tecnologias. Três principais tecnologias iriam imperar nesta época, eram elas: TDMA, CDMA e GSM. A segunda geração trouxe várias novidades. 

# SMS 
A primeira mensagem de texto foi enviada no ano de 1993, através de uma operadora da Finlândia. As mensagens de texto não eram grande coisa na época, porque eram limitadas a poucos caracteres e não permitiam a utilização de acentos ou caracteres especiais. Além disso, era difícil você poder utilizar o serviço de SMS, porque era necessário que, além do seu celular, o do destinatário fosse compatível com a tecnologia. 

# Introdução das cores 
No começo os celulares eram dispositivos monocromáticos que não produziam o que os nossos olhos podiam perceber. Os fabricantes introduziram visores que permitiam distinguir imagens, mas ainda parecia muito irreal 
Então surgiram celulares com quatro mil cores. Não demorou muito para que os os aparelhos ganhassem 64 mil cores e logo apareceram visores com 256 mil cores, oq deixou as imagens muito mais realistas. 

# Mensagens Multimídia e internet 
Com a possibilidade de visualizar imagens com cores não demorou muito para que surgissem as mensagens multimídia. As mensagens multimídia seriam uteis para enviar imagens, com a evolução, passou a suportar até mesmo envio de vídeos. 
A internet no celular era muito diferente da utilizada em computadores, mas isso deveria evoluir rapidamente. Era preciso criar páginas próprias para celular. 

# Geração 2,5 
a geração 2, 5G foi marcada por um aumento significativo na velocidade de acesso a internet, pelas novas características dos aparelhos e claro, por apresentar um novo conceito de celular aos usuários. 

# Câmera para imagens ou vídeos 
A implementação de uma câmera num celular foi muito revolucionária, mas até hoje é difícil encontrar algum aparelho que traga uma câmera de boa qualidade, ou que consiga resultados aceitáveis em qualquer situação. 

# Smartphones 
O termo smartphone foi adotado devido à utilização de um sistema operacional nos celulares. Além do sistema operacional, a maioria dos smartphones traz rede sem fio (wi-fi), câmera de qualidade razoável (geralmente o mínimo é 2 MP), Bluetooth (alguns aparelhos não são compatíveis com a tecnologia AD2P), memória interna com muito espaço — ou espaço para cartão externo —, funções aprimoradas, suporte a redes 3G e muito mais 

# Terceira Geração 
A terceira geração ofereceu várias vantagens, como: vídeochamada, conexão de internet de alta velocidade, economia de energia nos aparelhos e funcionalidade de internet sem a necessidade de um aparelho celular (é possível utilizar a rede de internet 3G em Modems). 

# Referência:
https://www.tecmundo.com.br/celular/2140-historia-a-evolucao-do-celular.htm
