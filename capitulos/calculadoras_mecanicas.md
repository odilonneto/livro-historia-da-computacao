# Calculadoras mecanicas

# Ábaco
A primeira máquina de contar foi o “ábaco”, instrumento de origem chinesa criado no século V a.C. Utilizado como uma espécie de calculadora, ele é considerado o primeiro computador.



![](https://static.todamateria.com.br/upload/ab/ac/abacoexemplo.jpg)

# Régua de cálculo
Buscando formas mais eficientes para efetuar cálculos, durante o século XVII os estudos do matemático John Naiper foram a base para a criação da régua de cálculo". O mecanismo consistia em uma régua que já tinha uma boa quantidade de valores pré-calculados, organizados de forma que fossem acessados automaticamente. Uma espécie de ponteiro indicava o resultado do valor desejado.




![](https://static.todamateria.com.br/upload/re/gu/reguadecalculoantiga.jpg)

# Máquina de Pascal
Pouco tempo depois da criação da “régua de cálculo” o matemático francês Blaise Pascal criou a primeira calculadora mecânica da história, a Máquina de Pascal. Seu funcionamento era baseado no uso de rodas interligadas que giravam na realização dos cálculos.



![](https://img.ibxk.com.br/materias/maquinapascalina.jpg?ims=704x)

# Primeira calculadora mecânica
Em 1947, o austríaco Curt Herzstark desenvolveu o projeto da primeira calculadora mecânica, reduzida ao tamanho de um copo. O desenvolvimento ocorreu enquanto Curt era prisioneiro no campo de concentração nazista. Porém, ele só conseguiu concluir seu projeto quando foi liberto. As vendas de suas calculadoras duraram até 1973, quando surgiram as calculadoras eletrônicas.




![](https://3.bp.blogspot.com/-SXyxgfpYGnQ/UEvz3FrXNNI/AAAAAAAAAZI/Ug9_nP14lng/s1600/443px-Curta_-_National_Museum_of_Computing.jpg)

# Calculadoras atuais
Hoje em dia, as calculadoras, além de executarem operações aritméticas básicas, podem executar funções trigonométricas normais e inversas, além de poderem armazenar dados e instruções em registros de memórias, aproximando-a de computadores menores. Muitas calculadoras utilizam módulos pré-programados e reversíveis de software, com mais de 5 mil instruções.



![](https://www.historiadetudo.com/wp-content/uploads/2015/03/calculadora-1.jpg)


# Referências:
https://www.todamateria.com.br/historia-e-evolucao-dos-computadores/

https://www.tecmundo.com.br/tecnologia-da-informacao/1697-a-historia-dos-computadores-e-da-computacao.htm
