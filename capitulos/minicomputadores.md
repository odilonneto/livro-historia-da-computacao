# Minicomputadores

Enquanto um minicomputador soa como um pequeno computador, o nome pode ser um pouco enganador. De fato, os minicomputadores são várias vezes o tamanho da área de trabalho PCs e são apenas um passo abaixo mainframes na hierarquia de classes de computador.

O termo "minicomputador" foi introduzido nos 1960s para descrever computadores poderosos que não eram tão grandes quanto os mainframes, que às vezes podiam preencher uma sala inteira. Em vez disso, a maioria dos minicomputadores tinha alguns metros de largura e vários metros de altura. Eles foram usados ​​principalmente por grandes empresas durante os 1960s e 1970s para processar grandes quantidades de dados. Alguns minicomputadores também funcionavam como servidores, permitindo que vários usuários os acessem a partir de terminais.

Como computador processadores tornou-se menor e mais poderoso, microcomputadores começou a rivalizar com minicomputadores em poder de processamento. Portanto, nos 1980s, os minicomputadores começaram a se tornar menos relevantes e eventualmente se tornaram obsoletos. Atualmente, os servidores baseados em rack executam funções semelhantes aos minicomputadores.

# Referências:

1. TechLib - O dicionario de informatica Lib Tech
