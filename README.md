# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice
![](https://3.bp.blogspot.com/-91Zmj87m1NY/VD8G0X-ixzI/AAAAAAAACeo/phO9DuYiPEM/s1600/SIMBOLO%2BENGENHARIA%2BDA%2BCOMPUTA%C3%87%C3%83O.jpg)
1. [História dos Computadores](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/historia_dos_computadores.md)
    - [Calculadoras mecanicas](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/calculadoras_mecanicas.md)
1. [Primeiros Computadores](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/primeiros_computadores.md)
1. [Categorias de Computadores](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/categorias_de_computadores.md)
    - [Mainframes](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/mainframe.md)
    - [Minicomputadores](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/minicomputadores.md)
    - [Microcomputadores](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/microcomputadores.md)
1. [Computação Móvel]()
    - [História da Computação Movel](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/Historia_da_computação_movel.md)
    - [Gerações de Dispositivos](https://gitlab.com/odilonneto/livro-historia-da-computacao/-/blob/master/capitulos/Gerações_de_dispositivos.md)





## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9982723/avatar.png?width=400)  | Odilon Neto | odilonneto | [odilonneto@alunos.utfpr.edu.br](mailto:odilonneto@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9906081/avatar.png?width=400)  | Mateus Rigon | MateusRlink | [mateuslink@alunos.utfpr.edu.br](mailto:mateuslink@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9980149/avatar.png?width=400)  | Leonardo Capelin |Leonardo_Capelin| [leonardocapelin@alunos.utfpr.edu.br](mailto:leonardocapelin@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9867470/avatar.png?width=400)  | Felipe Navarro | Chomray | [blasques@alunos.utfpr.edu.br](mailto:blasques@alunos.utfpr.edu.br)
